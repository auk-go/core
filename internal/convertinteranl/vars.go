package convertinteranl

var (
	Integers      = integersConverter{}
	AnyTo         = anyTo{}
	StringsTo     = stringsTo{}
	Map           = mapConverter{}
	KeyValuesTo   = keyValuesTo{}
	Util          = util{}
	CodeFormatter = codeFormatter{}
)
