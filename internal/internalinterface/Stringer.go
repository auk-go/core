package internalinterface

type Stringer interface {
	String() string
}
