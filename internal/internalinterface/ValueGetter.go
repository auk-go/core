package internalinterface

type ValueGetter interface {
	ValueDynamic() interface{}
}
