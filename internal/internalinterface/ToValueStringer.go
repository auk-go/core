package internalinterface

type ToValueStringer interface {
	Value() string
}
