package chmodhelpertestwrappers

import "gitlab.com/auk-go/core/coretests"

const (
	RwxApplyOnPath                coretests.TestFuncName = "RwxApplyOnPath"
	UnixRwxApplyRecursivelyOnPath coretests.TestFuncName = "UnixRwxApplyRecursivelyOnPath"
)
