package enumimpltests

import (
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/coredata/coredynamic"
	"gitlab.com/auk-go/core/coretests"
)

func Test_DynamicMapCreationDiff(t *testing.T) {
	for caseIndex, testCase := range dynamicMapDiffTestCases {
		// Arrange
		arrangeInput := testCase.ArrangeAsLeftRightDynamicMap()
		diffMap := arrangeInput.Left.DiffRaw(
			true,
			arrangeInput.Right,
		)
		mapAnyDiffer := coredynamic.MapAnyItemDiff(
			arrangeInput.Left,
		)

		// Act
		anotherDiff := mapAnyDiffer.
			DiffRaw(
				true,
				arrangeInput.Right,
			)

		// Assert
		testCase.ShouldBe(
			caseIndex,
			t,
			ShouldResemble,
			diffMap,
		)
		testCase.ShouldBeExplicit(
			false,
			caseIndex,
			t,
			"both diff should be equal",
			diffMap.Raw(),
			ShouldResemble,
			anotherDiff,
		)
	}
}

func Test_DynamicMapCreationDiffMessage(t *testing.T) {
	for caseIndex, testCase := range dynamicMapDiffMessageTestCases {
		// Arrange
		arrangeInput := testCase.ArrangeAsLeftRightDynamicMap()

		// Act
		diffJsonMessage := arrangeInput.Left.ShouldDiffMessage(
			true,
			testCase.CaseTitle(),
			arrangeInput.Right,
		)
		lines := coretests.GetAssert.ToStrings(diffJsonMessage)

		// Assert
		testCase.ShouldBe(
			caseIndex,
			t,
			ShouldResemble,
			lines,
		)
	}
}

func Test_DynamicMapCreationDiffMessageV2(t *testing.T) {
	for caseIndex, testCase := range dynamicMapDiffMessageTestCasesV2 {
		// Arrange
		arrangeInput := testCase.ArrangeAsLeftRightDynamicMapWithDefaultChecker()

		// Act
		diffJsonMessage := arrangeInput.
			Left.
			ShouldDiffLeftRightMessageUsingDifferChecker(
				arrangeInput.DifferChecker,
				true,
				testCase.CaseTitle(),
				arrangeInput.Right,
			)
		lines := strings.Split(
			diffJsonMessage,
			constants.NewLineUnix,
		)

		// Assert
		testCase.ShouldBe(
			caseIndex,
			t,
			ShouldResemble,
			lines,
		)
	}
}
