package enumimpltests

import "gitlab.com/auk-go/core/coreimpl/enumimpl"

type LeftRightDynamicMap struct {
	Left, Right enumimpl.DynamicMap
}
