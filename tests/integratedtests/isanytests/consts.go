package isanytests

const (
	booleanPrintFormatWithType          = "%d : %t (value: %v, type: %T)"
	defaultCaseIndexBoolStringFmt       = "%d : %t (%s)"
	defaultCaseIndexBoolStringStringFmt = "%d : %t (%s, %s)"
	conclusiveFormat                    = "%d - Equal : %t - %s (%s)"
	booleanTypeStringStringFormat       = "%d : %t (type: %T, %s, %s)"
)
