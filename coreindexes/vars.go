package coreindexes

var (
	indexByNameMap = map[int]string{
		First:   "First",
		Second:  "Second",
		Third:   "Third",
		Fourth:  "Fourth",
		Fifth:   "Fifth",
		Sixth:   "Sixth",
		Seventh: "Seventh",
		Eighth:  "Eighth",
		Ninth:   "Ninth",
		Tenth:   "Tenth",
	}
)
