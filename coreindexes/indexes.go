package coreindexes

//goland:noinspection ALL
const (
	First   = 0
	Second  = 1
	Third   = 2
	Fourth  = 3
	Fifth   = 4
	Sixth   = 5
	Seventh = 6
	Eighth  = 7
	Ninth   = 8
	Tenth   = 9
	I0      = 0
	I1      = 1
	I2      = 2
	I3      = 3
	I4      = 4
	I5      = 5
	I6      = 6
	I7      = 7
	I8      = 8
	I9      = 9
	I10     = 10
	I11     = 11
	I12     = 12
	I13     = 13
	I14     = 14
	I15     = 15
	I16     = 16
	I17     = 17
	I18     = 18
	I19     = 19
	I20     = 20
)
