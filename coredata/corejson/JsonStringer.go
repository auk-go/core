package corejson

type JsonStringer interface {
	JsonString() string
}
