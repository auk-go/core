package corejson

const (
	errTypeString  = "ErrorType.String"
	bytesFieldName = "Bytes"
	errorFieldName = "Error"
	typeFieldName  = "Type"
)
