package corejson

type JsonAnyModeler interface {
	JsonModelAny() interface{}
}
