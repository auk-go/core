package corejson

type PrettyJsonStringer interface {
	PrettyJsonString() string
}
