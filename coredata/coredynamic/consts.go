package coredynamic

import "gitlab.com/auk-go/core/constants"

const (
	supportedTypesMessageReference = " Supported Types: https://t.ly/SGWUx, "
	defaultMaxLevelOfReflection    = constants.DefaultMaxLevel
)
