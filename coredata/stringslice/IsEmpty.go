package stringslice

func IsEmpty(slice []string) bool {
	return len(slice) == 0
}
