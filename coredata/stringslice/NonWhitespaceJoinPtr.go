package stringslice

import (
	"strings"

	"gitlab.com/auk-go/core/constants"
)

func NonWhitespaceJoinPtr(slice *[]string, joiner string) string {
	if slice == nil {
		return constants.EmptyString
	}

	length := len(*slice)

	if length == 0 {
		return constants.EmptyString
	}

	return strings.Join(*NonWhitespacePtr(slice), joiner)
}
