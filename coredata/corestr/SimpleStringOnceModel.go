package corestr

type SimpleStringOnceModel struct {
	Value        string
	IsInitialize bool
}
