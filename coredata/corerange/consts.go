package corerange

import "gitlab.com/auk-go/core/constants"

const (
	defaultSeparator = constants.Pipe
)
