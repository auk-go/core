package coreinterface

type JsonStringer interface {
	JsonString() string
}
