package coreinterface

type CategoryRevealer interface {
	CategoryNamer
	FilterTextGetter
	EntityTypeNamer
}
