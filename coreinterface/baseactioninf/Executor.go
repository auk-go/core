package baseactioninf

type Executor interface {
	Execute() error
}
