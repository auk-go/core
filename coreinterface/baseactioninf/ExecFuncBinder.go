package baseactioninf

type ExecFuncBinder interface {
	Exec()
}
