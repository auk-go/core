package baseactioninf

type IsInvalidChecker interface {
	IsInvalid() bool
}
