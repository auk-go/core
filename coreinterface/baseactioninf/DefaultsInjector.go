package baseactioninf

type DefaultsInjector interface {
	InjectDefaults() error
}
