package coreinterface

type StringCompiler interface {
	Compile() string
}
