package coreinterface

import "gitlab.com/auk-go/core/internal/internalinterface"

type CommonSliceDefiner interface {
	internalinterface.CommonSliceDefiner
}
