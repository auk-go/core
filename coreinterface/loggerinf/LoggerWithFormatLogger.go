package loggerinf

type LoggerWithFormatLogger interface {
	Logger
	FormatLogger
}
