package loggerinf

import "gitlab.com/auk-go/core/internal/internalinterface"

type LoggerTyper interface {
	internalinterface.LoggerTyper
}
