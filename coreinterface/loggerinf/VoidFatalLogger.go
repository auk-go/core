package loggerinf

type VoidFatalLogger interface {
	LogFatal()
}
