package loggerinf

import "gitlab.com/auk-go/core/internal/internalinterface"

type LogTypeChecker interface {
	internalinterface.LogTypeChecker
}
