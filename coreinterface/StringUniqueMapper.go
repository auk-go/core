package coreinterface

type StringUniqueMapper interface {
	UniqueMap() map[string]bool
}
