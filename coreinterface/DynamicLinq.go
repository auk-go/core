package coreinterface

import "gitlab.com/auk-go/core/internal/internalinterface"

type DynamicLinq interface {
	CountGetter
	LengthGetter
	FirstDynamic() interface{}
	LastDynamic() interface{}
	FirstOrDefaultDynamic() interface{}
	LastOrDefaultDynamic() interface{}
	SkipDynamic(skippingItemsCount int) interface{}
	TakeDynamic(takeDynamicItems int) interface{}
	// LimitDynamic alias for TakeDynamic
	LimitDynamic(limit int) interface{}
}

type DyanmicLinqer interface {
	internalinterface.DyanmicLinqer
}
