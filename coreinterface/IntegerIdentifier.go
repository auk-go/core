package coreinterface

import "gitlab.com/auk-go/core/internal/internalinterface"

type IntegerIdentifier interface {
	internalinterface.IntegerIdGetter
}
