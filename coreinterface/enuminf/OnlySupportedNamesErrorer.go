package enuminf

import "gitlab.com/auk-go/core/internal/internalinterface/internalenuminf"

type OnlySupportedNamesErrorer interface {
	internalenuminf.OnlySupportedNamesErrorer
}
