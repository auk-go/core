package enuminf

import "gitlab.com/auk-go/core/internal/internalinterface"

type SimpleEnumer interface {
	internalinterface.SimpleEnumer
}
