package pathextendinf

type PathExtenderGetter interface {
	PathExtender() PathExtender
}
