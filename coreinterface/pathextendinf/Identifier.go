package pathextendinf

import "gitlab.com/auk-go/core/internal/internalinterface/internalpathextender"

type Identifier interface {
	internalpathextender.Identifier
}
