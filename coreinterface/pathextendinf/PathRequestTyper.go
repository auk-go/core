package pathextendinf

import "gitlab.com/auk-go/core/internal/internalinterface/internalpathextender"

type PathRequestTyper interface {
	internalpathextender.PathRequestTyper
}
