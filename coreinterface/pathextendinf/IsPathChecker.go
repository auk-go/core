package pathextendinf

import "gitlab.com/auk-go/core/internal/internalinterface/internalpathextender"

type IsPathChecker interface {
	internalpathextender.IsPathChecker
}
