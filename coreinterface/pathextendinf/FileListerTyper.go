package pathextendinf

import "gitlab.com/auk-go/core/internal/internalinterface/internalpathextender"

type FileListerTyper interface {
	internalpathextender.FileListerTyper
}
