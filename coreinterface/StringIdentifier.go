package coreinterface

type StringIdentifier interface {
	IdString() string
}
