package coreinterface

type Stringer interface {
	String() string
}
