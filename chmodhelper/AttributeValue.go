package chmodhelper

type AttributeValue struct {
	Read, Write, Execute, Sum byte
}
