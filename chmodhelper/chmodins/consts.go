package chmodins

const (
	RwxFullLength             = 10
	RwxWithoutHyphenLength    = 9
	RwxFullLengthString       = "10"
	AllWildCardsRwxFullString = "-*********"
)
