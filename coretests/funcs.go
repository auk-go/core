package coretests

type (
	ToLineConverterFunc func(i int, spacePrefix, line string) string
)
