package args

import "gitlab.com/auk-go/core/constants"

const (
	// newLineSpaceIndent - "\n   - "
	newLineSpaceIndent = constants.NewLineBulletWithSpaceIndent
	selfToStringFmt    = "%s { %s }"
	inArgNamePrefix    = "inArg"
	outArgNamePrefix   = "r"
)
