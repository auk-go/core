package coreversion

import "gitlab.com/auk-go/core/constants"

const (
	VSymbol             = "v"
	InvalidVersionValue = constants.InvalidValue
)
