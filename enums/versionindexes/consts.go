package versionindexes

const (
	BuildLength       = 4
	PatchLength       = 3
	MinorLength       = 2
	MajorLength       = 1
	NoElementLength   = 0
	isMappedToDefault = true
)
