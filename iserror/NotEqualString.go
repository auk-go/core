package iserror

func NotEqualString(left, right string) bool {
	return left != right
}
