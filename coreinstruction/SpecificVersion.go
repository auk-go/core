package coreinstruction

type SpecificVersion struct {
	Version    string `json:"SpecificVersion"`
	IsSpecific bool   `json:"IsSpecific"`
}
