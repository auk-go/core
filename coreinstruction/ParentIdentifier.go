package coreinstruction

type ParentIdentifier struct {
	ParentId      string `json:"ParentId,omitempty"`
	ParentName    string `json:"ParentName,omitempty"`
	ParentVersion string `json:"ParentVersion,omitempty"`
}
