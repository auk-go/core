package testconsts

//goland:noinspection ALL
const (
	JohnDoe     = "John Doe"
	JaneDoe     = "Jane Doe"
	John        = "John"
	Jane        = "Jane"
	Doe         = "Doe"
	NumberOne   = 1
	NumberTwo   = 2
	NumberThree = 3
	NumberFive  = 5
)
