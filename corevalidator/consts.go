package corevalidator

const (
	actualUserInputsMessage         = "Actual Received: "
	numberWithHeaderFormat          = "%d ) %s"
	actualUserInputsV2MessageFormat = "%d ) Actual Received:\n    %s"
	expectingLinesV2MessageFormat   = "%d )  Expected Input:\n     %s"
	expectingLinesMessage           = "Expecting Input: "
)
