package codegen

import "gitlab.com/auk-go/core/constants"

const (
	ArgsJoiner       = constants.CommaSpace
	fmtJoiner        = constants.CommaUnixNewLine
	totalSliceLength = "40"
	newLine          = constants.NewLineUnix
)
