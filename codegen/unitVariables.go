package codegen

type unitVariables struct {
	PackageName      string
	NewPackages      string
	FuncName         string
	ArrangeType      string
	LinesPossible    string
	ActArgsSetup     string
	InArgs           string
	OutArgs          string
	FmtJoin          string
	FmtOutputs       string
	Behaviour        string
	TestCaseName     string
	DirectFuncInvoke string
	Title            string
	ArrangeSetup     string
	ExpectedLines    string
	CaseItem         string
	TestCases        string
	VerifyTypeOf     string
	VariablesSetup   string
	workFunc         string
	expect           string
	inputExpectedVar string
}
