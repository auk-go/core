package reflectmodel

import "gitlab.com/auk-go/core/constants"

const (
	newLineSpaceIndent = constants.NewLineBulletWithSpaceIndent
)
