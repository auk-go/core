package coremath

type isRangeWithin struct {
	Integer           integerWithin
	Integer16         integer16Within
	Integer32         integer32Within
	Integer64         integer64Within
	UnsignedInteger16 unsignedInteger16Within
}
