package coremath

func MinInt(int1, int2 int) int {
	if int1 > int2 {
		return int2
	}

	return int1
}
