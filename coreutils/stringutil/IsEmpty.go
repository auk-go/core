package stringutil

func IsEmpty(str string) bool {
	return str == ""
}
