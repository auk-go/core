package stringutil

import "gitlab.com/auk-go/core/constants"

const (
	ExpectedLeftRightLength = constants.Two
)
