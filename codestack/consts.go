package codestack

import "gitlab.com/auk-go/core/constants"

const (
	funcPrintFormat     = "func %s (...) line (%d):\n\r\tFile %s:%d"
	shortStringFormat   = "%s (%d) -> %s:%d"
	fileWithLineFormat  = "%s:%d"
	DefaultStackCount   = 12
	defaultInternalSkip = constants.One
	Take2               = 2
	Take5               = 5
	Take6               = 6
	Take8               = 8
	Take10              = 10
	Take12              = 12
	Take15              = 15
	Take20              = 20
	Take25              = 25
	Take50              = 50
	Take100             = 100
	Take200             = 200
	SkipNone            = constants.Zero
	Skip1               = constants.One
	Skip2               = constants.Two
	Skip3               = constants.Three
	Skip4               = constants.Four
	Skip5               = constants.Five
	Skip6               = constants.Six
	Skip7               = constants.Seven
	Skip8               = constants.Eight
	Skip9               = constants.Nine
	Skip10              = constants.Ten
	Skip11              = 11
	Skip12              = 12
	Skip13              = 13
	Skip14              = 14
	Skip15              = 15
	Skip16              = 16
	Skip17              = 17
	Skip18              = 18
	Skip19              = 19
	Skip20              = 20
)
