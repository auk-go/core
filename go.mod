module gitlab.com/auk-go/core

go 1.17.8

require (
	github.com/smarty/assertions v1.15.1
	github.com/smartystreets/goconvey v1.8.1
)

require (
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
)
