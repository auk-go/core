package keymk

// TempReplace
//
//	Refers to Template replace request
type TempReplace struct {
	KeyName string
	Range   Range
}
