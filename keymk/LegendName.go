package keymk

type LegendName struct {
	Root, Group, Package,
	State, User, Item string
}
